const String listCoffees = r'''
query ListCoffees {
  listCoffees(filter: { deleted: { eq: false } }) {
    items {
      id
      debtorId
      withCake
      withCoffee
      date
      deleted
      latitude
      longitude
    }
  }
}
''';

const String listDebtors = r'''
query ListDebtors {
  listDebtors {
    items {
      id
      color
    }
  }
}
''';
