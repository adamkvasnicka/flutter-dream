const String createOrder = r'''
  mutation CreateCoffe($id: ID!, $debtorId: ID, $withCoffee: Boolean, $withCake: Boolean, $date: String, $latitude: String, $longitude: String) {
  createCoffee(input: {
    id: $id,
    debtorId: $debtorId,
    withCoffee: $withCoffee,
    withCake: $withCake,
    latitude: $latitude,
    longitude: $longitude,
    date: $date
    deleted: false
  }) {
    id
    debtorId
    date
    withCake
    withCoffee
    deleted
  }
}
''';

const String deleteCoffee = r'''
mutation DeleteCoffee($id: ID!) {
  updateCoffee(input: {
    id: $id,
    deleted: true,
  }) {
     	id
      debtorId
      withCake
      withCoffee
      date
      deleted
  }
}
''';
