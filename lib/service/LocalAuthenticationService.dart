import 'dart:io';
import 'package:local_auth/local_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalAuthenticationService {
  static String key = "protection.enabled";

  final _auth = LocalAuthentication();
  bool isAuthenticated = false;

  Future<bool> authenticate() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var securityEnabled = prefs.get(LocalAuthenticationService.key);
    if ((securityEnabled == 'true' ? true : false) && !Platform.isIOS) {
      try {
        isAuthenticated = await _auth.authenticateWithBiometrics(
          localizedReason: 'Authenticate',
          useErrorDialogs: true,
          stickyAuth: true,
        );
        return isAuthenticated;
      } catch (e) {
        print(e);
      }
    }
    return true;
  }
}

GetIt locator = GetIt.I;

void setupLocator() {
  locator.registerLazySingleton(() => LocalAuthenticationService());
}
