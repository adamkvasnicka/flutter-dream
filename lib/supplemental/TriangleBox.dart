import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TriangleBoxPainter extends CustomPainter {
  Paint _paint;

  TriangleBoxPainter(Color color) {
    _paint = Paint()
      ..color = color
      ..style = PaintingStyle.fill;
  }

  void paint(Canvas canvas, Size size) {
    Path path = Path();
    path.lineTo(40, 0);
    path.lineTo(0, 40);
    path.close();
    canvas.drawPath(path, _paint);
  }

  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}
