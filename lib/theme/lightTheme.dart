import 'package:coffee_debtor/supplemental/CutCornersBorder.dart';
import 'package:flutter/material.dart';

ThemeData lightTheme = ThemeData(
  primarySwatch: Colors.brown,
  accentColor: Colors.white,
  primaryIconTheme: IconThemeData(color: Colors.white),
  primaryTextTheme: TextTheme(
    body2: TextStyle(
      color: Colors.black,
    ),
    body1: TextStyle(
      color: Colors.black,
      fontWeight: FontWeight.bold,
      fontSize: 18.0,
      height: 1.6,
    ),
  ),
  backgroundColor: Colors.white,
  cardColor: Colors.white,
  accentIconTheme: IconThemeData(color: Colors.brown[900]),
  inputDecorationTheme: InputDecorationTheme(
    border: CutCornersBorder(
      borderRadius: const BorderRadius.all(
        const Radius.circular(1.0),
      ),
      borderSide: new BorderSide(
        color: Colors.brown[300],
        width: 1.0,
      ),
    ),
    focusedBorder: CutCornersBorder(
      borderRadius: const BorderRadius.all(
        const Radius.circular(2.0),
      ),
      borderSide: new BorderSide(
        color: Colors.brown[600],
        width: 1.0,
      ),
    ),
  ),
);
