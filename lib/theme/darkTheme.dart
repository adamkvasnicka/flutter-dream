import 'package:coffee_debtor/supplemental/CutCornersBorder.dart';
import 'package:flutter/material.dart';

ThemeData darkTheme = ThemeData(
  primarySwatch: Colors.brown,
  accentColor: Colors.black,
  primaryIconTheme: IconThemeData(color: Colors.black),
  primaryTextTheme: TextTheme(
    body2: TextStyle(
      color: Colors.brown[100],
    ),
    body1: TextStyle(
      color: Colors.brown[100],
      fontWeight: FontWeight.bold,
      fontSize: 18.0,
      height: 1.6,
    ),
  ),
  backgroundColor: Colors.black,
  cardColor: Colors.brown[800],
  accentIconTheme: IconThemeData(color: Colors.brown[400]),
  inputDecorationTheme: InputDecorationTheme(
    border: CutCornersBorder(
      borderRadius: const BorderRadius.all(
        const Radius.circular(1.0),
      ),
      borderSide: new BorderSide(
        color: Colors.brown[100],
        width: 1.0,
      ),
    ),
    focusedBorder: CutCornersBorder(
      borderRadius: const BorderRadius.all(
        const Radius.circular(2.0),
      ),
      borderSide: new BorderSide(
        color: Colors.brown[300],
        width: 1.0,
      ),
    ),
  ),
);
