import 'package:coffee_debtor/service/LocalAuthenticationService.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SecurityNotifier with ChangeNotifier {
  SharedPreferences prefs;

  SecurityNotifier(this.prefs);

  bool getSecurityEnabled() {
    var securityEnabled = prefs.get(LocalAuthenticationService.key);
    return securityEnabled == 'true' ? true : false;
  }

  void toggleSecurity() {
    var securityEnabled = prefs.get(LocalAuthenticationService.key) ?? 'false';
    prefs.setString(LocalAuthenticationService.key,
        securityEnabled == 'true' ? 'false' : 'true');
    notifyListeners();
  }
}
