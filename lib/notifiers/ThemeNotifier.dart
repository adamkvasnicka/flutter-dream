import 'package:coffee_debtor/theme/darkTheme.dart';
import 'package:coffee_debtor/theme/lightTheme.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeNotifier with ChangeNotifier {
  SharedPreferences prefs;
  final _key = "theme.current";
  ThemeTypes actualTheme;

  ThemeNotifier(this.prefs);

  ThemeData getTheme() {
    var theme = prefs.get(_key);
    actualTheme = theme == ThemeTypes.DARK.toString()
        ? ThemeTypes.DARK
        : ThemeTypes.LIGHT;
    return actualTheme == ThemeTypes.DARK ? darkTheme : lightTheme;
  }

  void switchTheme() {
    if (actualTheme == ThemeTypes.DARK) {
      actualTheme = ThemeTypes.LIGHT;
    } else {
      actualTheme = ThemeTypes.DARK;
    }
    prefs.setString(
        _key,
        actualTheme == ThemeTypes.DARK
            ? ThemeTypes.DARK.toString()
            : ThemeTypes.LIGHT.toString());
    notifyListeners();
  }
}

enum ThemeTypes { DARK, LIGHT }
