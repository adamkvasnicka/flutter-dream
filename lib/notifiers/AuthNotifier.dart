import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:coffee_debtor/helper/SecretCognitoStorage.dart';
import 'package:flutter/material.dart';
import 'package:lombok/lombok.dart';

@data
class AuthNotifier with ChangeNotifier {
  CognitoUser cognitoUser;
  CognitoUserSession session;
  CognitoUserPool userPool = new CognitoUserPool(
      'eu-central-1_oQ0vboJey', '75l1952atlm3qjevvifiphmi8h',
      storage: SecretCognitoStorage());

  CognitoUser createUser(String username) {
    cognitoUser = CognitoUser(username, userPool);
    userPool.storage.setItem("cognitoUsername", username);
    return cognitoUser;
  }

  Future<CognitoUserSession> getRefreshedSession() async {
    if (this.cognitoUser == null) {
      var username = await userPool.storage.getItem("cognitoUsername");
      this.cognitoUser =
          CognitoUser(username, userPool, storage: userPool.storage);
      var authenticationDetails =
          AuthenticationDetails(username: username, password: 'Test1234.');
      await cognitoUser.authenticateUser(authenticationDetails);
    }
    return Future.delayed(
        Duration(seconds: 4), () => this.cognitoUser.getSession());
  }

  void setSession(CognitoUserSession session) {
    this.session = session;
    notifyListeners();
  }

  void clearSession() {
    this.session = null;
    this.cognitoUser = null;
    userPool.storage.clear();
    notifyListeners();
  }
}
