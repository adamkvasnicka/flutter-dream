import 'package:coffee_debtor/models/CoffeeList.dart';
import 'package:coffee_debtor/screens/CakeScreen.dart';
import 'package:coffee_debtor/screens/CoffeeScreen.dart';
import 'package:coffee_debtor/screens/StatesScreen.dart';
import 'package:coffee_debtor/screens/SettingsScreen.dart';
import 'package:coffee_debtor/widgets/CoffeeQuery.dart';
import 'package:flutter/material.dart';

class RouteNotifier with ChangeNotifier {
  CoffeeQueryWidgetCallback currentRoute;
  CoffeeQueryWidgetCallback backRoute;
  int currentIndex;
  int backIndex;

  RouteNotifier()
      : currentIndex = 0,
        backIndex = 0,
        backRoute = routes[0],
        currentRoute = routes[0];

  void changeRoute(int route) {
    if (route < routes.length && route >= 0) {
      backRoute = currentRoute;
      currentRoute = routes[route];
      backIndex = currentIndex;
      currentIndex = route;
      notifyListeners();
    }
  }
}

List<CoffeeQueryWidgetCallback> routes = [
  (CoffeeList items, Function() refetch) => CoffeeScreen(coffeeList: items, refetch: refetch),
  (CoffeeList items, Function() refetch) => CakeScreen(coffeeList: items, refetch: refetch),
  (CoffeeList items, Function() refetch) => StatesScreen(coffeeList: items, refetch: refetch),
  (CoffeeList items, Function() refetch) => SettingsScreen(key: UniqueKey()),
];
