import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:camera/camera.dart';
import 'package:coffee_debtor/notifiers/RouteNotifier.dart';
import 'package:coffee_debtor/widgets/CoffeeDebtor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:coffee_debtor/notifiers/SecurityNotifier.dart';
import 'package:coffee_debtor/screens/LoginScreen.dart';
import 'package:coffee_debtor/screens/FullScreenLoader.dart';
import 'package:coffee_debtor/screens/BiometricsScreen.dart';
import 'package:coffee_debtor/service/LocalAuthenticationService.dart';
import 'package:coffee_debtor/notifiers/AuthNotifier.dart';
import 'package:coffee_debtor/notifiers/ThemeNotifier.dart';

List<CameraDescription> cameras;

Future<Null> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  setupLocator();
  cameras = await availableCameras();
  runApp(MyApp(prefs));
}

final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

class MyApp extends StatelessWidget {
  final SharedPreferences prefs;

  MyApp(this.prefs);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<ThemeNotifier>(
      create: (_) => ThemeNotifier(prefs),
      child: Consumer<ThemeNotifier>(
        builder: (context, themeNotifier, _) => MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'CoffeDebtor',
            theme: themeNotifier.getTheme(),
            initialRoute: '/',
            navigatorObservers: [routeObserver],
            home: MultiProvider(
              providers: [
                ChangeNotifierProvider(create: (_) => AuthNotifier()),
                ChangeNotifierProvider(create: (_) => SecurityNotifier(prefs))
              ],
              child: Main(),
            )),
      ),
    );
  }
}

class Main extends StatelessWidget {
  Widget build(BuildContext context) {
    return Consumer<AuthNotifier>(
      builder: (context, auth, _) => Conditional.single(
        context: context,
        conditionBuilder: (context) => auth.session != null,
        widgetBuilder: (context) => ChangeNotifierProvider<RouteNotifier>(
            create: (_) => RouteNotifier(),
            child: Consumer<RouteNotifier>(
                builder: (context, _routes, _) => CoffeeDebtor(_routes))),
        fallbackBuilder: (context) => FutureBuilder(
            future: auth.getRefreshedSession(),
            builder: (BuildContext context,
                AsyncSnapshot<CognitoUserSession> snapshot) {
              if (snapshot.hasData) {
                auth.setSession(snapshot.data);
                return BiometricsScreen();
              } else if (snapshot.hasError) {
                return LoginScreen();
              } else {
                return FullScreenLoader();
              }
            }),
      ),
    );
  }
}
