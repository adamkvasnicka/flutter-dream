// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Debtor.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Debtor _$DebtorFromJson(Map<String, dynamic> json) {
  return Debtor(
    json['id'] as int,
    json['color'] as String,
  );
}

Map<String, dynamic> _$DebtorToJson(Debtor instance) => <String, dynamic>{
      'id': instance.id,
      'color': instance.color,
    };
