import 'package:json_annotation/json_annotation.dart';

import './Coffee.dart';

part 'CoffeeList.g.dart';

bool notDeleted(Coffee coffee) => !coffee.deleted;

bool withCoffee(Coffee coffee) => coffee.withCoffee ?? false;

bool withCake(Coffee coffee) => coffee.withCake ?? false;

@JsonSerializable()
class CoffeeList {
  List<Coffee> items;

  CoffeeList(this.items);

  factory CoffeeList.fromJson(Map<String, dynamic> json) =>
      _$CoffeeListFromJson(json);

  Map<String, dynamic> toJson() => _$CoffeeListToJson(this);

  Coffee getCoffee(int index) => items[index];

  void sortItems() {
    items.sort((a, b) =>
        DateTime.parse(b.date).difference(DateTime.parse(a.date)).inSeconds);
  }

  List<Coffee> getNotDeletedCoffees() => items.where(notDeleted).toList();

  List<Coffee> getWithCoffee() =>
      items.where(notDeleted).where(withCoffee).toList();

  List<Coffee> getWithCake() =>
      items.where(notDeleted).where(withCake).toList();

  int getCount() => items.where(notDeleted).toList().length;
}
