// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'CoffeeList.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CoffeeList _$CoffeeListFromJson(Map<String, dynamic> json) {
  return CoffeeList(
    (json['items'] as List)
        ?.map((e) =>
            e == null ? null : Coffee.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$CoffeeListToJson(CoffeeList instance) =>
    <String, dynamic>{
      'items': instance.items,
    };
