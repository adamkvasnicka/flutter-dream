// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Coffee.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Coffee _$CoffeeFromJson(Map<String, dynamic> json) {
  return Coffee(
    json['id'] as String,
    json['debtorId'] as String,
    json['withCoffee'] as bool,
    json['withCake'] as bool,
    json['date'] as String,
    json['deleted'] as bool,
    json['latitude'] as String,
    json['longitude'] as String,
  );
}

Map<String, dynamic> _$CoffeeToJson(Coffee instance) => <String, dynamic>{
      'id': instance.id,
      'debtorId': instance.debtorId,
      'withCoffee': instance.withCoffee,
      'withCake': instance.withCake,
      'date': instance.date,
      'deleted': instance.deleted,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
