import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Coffee.g.dart';

@JsonSerializable()
class Coffee {
  String id;
  String debtorId;
  bool withCoffee;
  bool withCake;
  String date;
  bool deleted;
  String latitude;
  String longitude;

  Coffee(this.id, this.debtorId, this.withCoffee, this.withCake, this.date,
      this.deleted, this.latitude, this.longitude);

  factory Coffee.fromJson(Map<String, dynamic> json) => _$CoffeeFromJson(json);

  Map<String, dynamic> toJson() => _$CoffeeToJson(this);

  String getFormatedDateString() =>
      DateFormat.yMMMMd().format(DateTime.parse(date)).toString();
}
