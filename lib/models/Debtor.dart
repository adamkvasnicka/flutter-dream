import 'package:json_annotation/json_annotation.dart';

part 'Debtor.g.dart';

@JsonSerializable()
class Debtor {
  int id;
  String color;

  Debtor(this.id, this.color);

  factory Debtor.fromJson(Map<String, dynamic> json) => _$DebtorFromJson(json);
  Map<String, dynamic> toJson() => _$DebtorToJson(this);
}
