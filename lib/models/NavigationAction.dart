import 'package:flutter/widgets.dart';

class NavigationAction {
  final Function fnc;
  final Icon icon;

  NavigationAction(this.fnc, this.icon);

}
