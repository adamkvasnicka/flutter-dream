import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/material.dart';

final String URI =
    "https://u4df2yjlnfea7eysoznke5wyyu.appsync-api.eu-central-1.amazonaws.com/graphql";

final OptimisticCache cache = OptimisticCache(
  dataIdFromObject: typenameDataIdFromObject,
);

ValueNotifier<GraphQLClient> clientFor({
  String subscriptionUri,
  String accessToken
}) {
  Link link = HttpLink(uri: URI, headers: {"Authorization": accessToken});
  if (subscriptionUri != null) {
    final WebSocketLink websocketLink = WebSocketLink(
      url: subscriptionUri,
      config: SocketClientConfig(
        autoReconnect: true,
        inactivityTimeout: Duration(seconds: 30),
      ),
    );

    link = link.concat(websocketLink);
  }

  return ValueNotifier<GraphQLClient>(
    GraphQLClient(
      cache: cache,
      link: link,
    ),
  );
}

class ClientProvider extends StatelessWidget {
  ClientProvider({
    @required this.child,
    String subscriptionUri,
    String accessToken,
  }) : client = clientFor(
    subscriptionUri: subscriptionUri,
    accessToken: accessToken,
  );

  final Widget child;
  final ValueNotifier<GraphQLClient> client;

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: client,
      child: child,
    );
  }
}
