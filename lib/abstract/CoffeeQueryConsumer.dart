import 'package:flutter/cupertino.dart';

import '../models/CoffeeList.dart';

abstract class CoffeeQueryConsumer extends StatelessWidget {
  CoffeeList coffeeList;
  Function() refetch;

  CoffeeQueryConsumer(
      {Key key, @required this.coffeeList, @required this.refetch})
      : super(key: key);
}
