import 'package:coffee_debtor/graphql/mutations/mutations.dart';
import 'package:coffee_debtor/models/Coffee.dart';
import 'package:coffee_debtor/widgets/google_map_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class OrderTile extends StatelessWidget {
  final Coffee coffee;
  final VoidCallback refetch;
  bool _isAdam;

  OrderTile({this.coffee, this.refetch}) {
    this._isAdam = this.coffee.debtorId == "1";
  }

  Widget build(BuildContext context) {
    return Mutation(
        options: MutationOptions(
            documentNode: gql(deleteCoffee),
            update: (Cache cache, QueryResult result) {
              if (result.hasException) {
                print(['optimistic', result.exception.toString()]);
              } else {
                final Map<String, Object> updated =
                    Map<String, Object>.from(coffee.toJson())
                      ..addAll({'deleted': true});
                cache.write(typenameDataIdFromObject(updated), updated);
              }
            },
            onCompleted: (dynamic resultData) => refetch()),
        builder: (
          RunMutation runMutation,
          QueryResult result,
        ) =>
            GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return SimpleDialog(
                          backgroundColor: Colors.transparent,
                          children: [
                            GoogleMapView(
                                latitude: coffee.latitude,
                                longitude: coffee.longitude)
                          ]);
                    });
              },
              child: Padding(
                  padding: EdgeInsets.only(bottom: 5.0),
                  child: Dismissible(
                    background: Card(
                      shape: BeveledRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(7))),
                      color: Colors.red,
                      child: Container(
                        alignment: Alignment.centerRight,
                        padding: EdgeInsets.fromLTRB(25.0, 0.0, 25.0, 0.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(
                              Icons.delete,
                              color: Colors.white,
                            ),
                            Icon(
                              Icons.delete,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                    ),
                    key: Key(coffee.id),
                    onDismissed: (direction) async {
                      runMutation(
                        {
                          'id': coffee.id,
                        },
                        optimisticResult: {
                          'action': {'deleted': true}
                        },
                      );
                    },
                    child: Card(
                      shape: BeveledRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(7))),
                      color: Theme.of(context).cardColor,
                      child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 12.0, horizontal: 16.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                _isAdam ? "Adam" : "Vladimír",
                                style: Theme.of(context).primaryTextTheme.body1,
                              ),
                              Row(
                                children: [
                                  Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Text(
                                      coffee.getFormatedDateString(),
                                      style: Theme.of(context)
                                          .primaryTextTheme
                                          .body2,
                                    ),
                                  )
                                ],
                              ),
                            ],
                          )),
                    ),
                  )),
            ));
  }
}
