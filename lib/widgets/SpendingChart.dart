import 'package:coffee_debtor/models/Coffee.dart';
import 'package:coffee_debtor/models/CoffeeList.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

double coffeeCountForUserId(String userId, List<Coffee> coffees) => coffees
    .where((Coffee coffee) => coffee.debtorId == userId)
    .length
    .toDouble();

class SpendingChart extends StatefulWidget {
  CoffeeList coffeeList;

  SpendingChart({this.coffeeList});

  @override
  State<StatefulWidget> createState() => _SpendingChartState();
}

class _SpendingChartState extends State<SpendingChart> {
  Color leftBarColor = Color(0xff53fdd7);
  Color rightBarColor = Color(0xffff5182);
  double width = 7;

  List<BarChartGroupData> rawBarGroups;
  List<BarChartGroupData> showingBarGroups;

  int touchedGroupIndex;

  @override
  void initState() {
    super.initState();
    final items = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        .map((int month) => makeGroupData(month))
        .toList();
    rawBarGroups = items;
    showingBarGroups = rawBarGroups;
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
        color: Color(0xff2c4260),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Icon(
                    Icons.money_off,
                    color: Colors.white,
                  ),
                  SizedBox(
                    width: 38,
                  ),
                  Text(
                    'Spendings',
                    style: TextStyle(color: Colors.white, fontSize: 22),
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Text(
                    'state',
                    style: TextStyle(color: Color(0xff77839a), fontSize: 16),
                  ),
                ],
              ),
              SizedBox(
                height: 38,
              ),
              Expanded(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                  child: BarChart(
                    BarChartData(
                      maxY: 20,
                      barTouchData: BarTouchData(
                          touchTooltipData: BarTouchTooltipData(
                            tooltipBgColor: Colors.grey,
                            getTooltipItem: (_a, _b, _c, _d) => null,
                          ),
                          touchCallback: (response) {
                            if (response.spot == null) {
                              setState(() {
                                touchedGroupIndex = -1;
                                showingBarGroups = List.of(rawBarGroups);
                              });
                              return;
                            }

                            touchedGroupIndex =
                                response.spot.touchedBarGroupIndex;

                            setState(() {
                              if (response.touchInput is FlLongPressEnd ||
                                  response.touchInput is FlPanEnd) {
                                touchedGroupIndex = -1;
                                showingBarGroups = List.of(rawBarGroups);
                              } else {
                                showingBarGroups = List.of(rawBarGroups);
                                if (touchedGroupIndex != -1) {
                                  double sum = 0;
                                  for (BarChartRodData rod
                                      in showingBarGroups[touchedGroupIndex]
                                          .barRods) {
                                    sum += rod.y;
                                  }
                                  final avg = sum /
                                      showingBarGroups[touchedGroupIndex]
                                          .barRods
                                          .length;

                                  showingBarGroups[touchedGroupIndex] =
                                      showingBarGroups[touchedGroupIndex]
                                          .copyWith(
                                    barRods: showingBarGroups[touchedGroupIndex]
                                        .barRods
                                        .map((rod) {
                                      return rod.copyWith(y: avg);
                                    }).toList(),
                                  );
                                }
                              }
                            });
                          }),
                      titlesData: FlTitlesData(
                        show: true,
                        bottomTitles: SideTitles(
                          showTitles: true,
                          textStyle: TextStyle(
                              color: Color(0xff7589a2),
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                          margin: 10,
                          getTitles: (double value) {
                            switch (value.toInt()) {
                              case 0:
                                return 'Jan';
                              case 1:
                                return 'Feb';
                              case 2:
                                return 'Mar';
                              case 3:
                                return 'Apr';
                              case 4:
                                return 'May';
                              case 5:
                                return 'Jun';
                              case 6:
                                return 'Jul';
                              case 7:
                                return 'Aug';
                              case 8:
                                return 'Sep';
                              case 9:
                                return 'Oct';
                              case 10:
                                return 'Nov';
                              case 11:
                                return 'Dec';
                              default:
                                return '';
                            }
                          },
                        ),
                        leftTitles: SideTitles(
                          showTitles: true,
                          textStyle: TextStyle(
                              color: Color(0xff7589a2),
                              fontWeight: FontWeight.bold,
                              fontSize: 12),
                          margin: 5,
                          reservedSize: 14,
                          getTitles: (value) {
                            if (value == 0) {
                              return '5';
                            } else if (value == 10) {
                              return '10';
                            } else if (value == 20) {
                              return '20';
                            } else {
                              return '';
                            }
                          },
                        ),
                      ),
                      borderData: FlBorderData(
                        show: false,
                      ),
                      barGroups: showingBarGroups,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 12,
              ),
            ],
          ),
        ),
      ),
    );
  }

  BarChartGroupData makeGroupData(int x) {
    List<Coffee> coffeesFortMonth = widget.coffeeList
        .getWithCoffee()
        .where(
          (Coffee coffee) => DateTime.parse(coffee.date).month - 1 == x,
        )
        .toList();
    return BarChartGroupData(barsSpace: 2, x: x, barRods: [
      BarChartRodData(
        y: coffeeCountForUserId("1", coffeesFortMonth),
        color: leftBarColor,
        width: width,
      ),
      BarChartRodData(
        y: coffeeCountForUserId("2", coffeesFortMonth),
        color: rightBarColor,
        width: width,
      ),
    ]);
  }
}
