import 'dart:math';

import 'package:coffee_debtor/models/NavigationAction.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import 'NavigationButton.dart';
import 'NavigationPainter.dart';

class NavigationBar extends StatefulWidget {
  final List<Widget> items;
  final List<NavigationAction> actions;
  final int index;
  final Color color;
  final Color buttonBackgroundColor;
  final Color backgroundColor;
  final ValueChanged<int> onTap;
  final Curve animationCurve;
  final Duration animationDuration;
  final Duration actionDuration;
  final double height;

  NavigationBar({
    Key key,
    @required this.items,
    this.actions,
    this.index = 0,
    this.color = Colors.white,
    this.buttonBackgroundColor,
    this.backgroundColor = Colors.blueAccent,
    this.onTap,
    this.animationCurve = Curves.easeOut,
    this.animationDuration = const Duration(milliseconds: 600),
    this.actionDuration = const Duration(milliseconds: 900),
    this.height = 50.0,
  })  : assert(items != null),
        assert(items.length >= 1),
        assert(items.length == actions.length),
        assert(0 <= index && index < items.length),
        assert(0 <= height && height <= 75.0),
        super(key: key);

  @override
  NavigationBarState createState() => NavigationBarState();
}

class NavigationBarState extends State<NavigationBar>
    with TickerProviderStateMixin {
  double _startingPos;
  int _endingIndex = 0;
  double _pos;
  double _buttonHide = 0;
  Widget _icon;
  Widget _actionIcon;
  Function _actionFnc;
  AnimationController _animationController;
  AnimationController _rotationController;
  int _length;
  double _rotation;
  Animation<double> _secondAnimation;

  @override
  void initState() {
    super.initState();
    _icon = widget.items[widget.index];
    _actionIcon = widget.actions[widget.index].icon;
    _actionFnc = widget.actions[widget.index].fnc;
    _length = widget.items.length;
    _pos = widget.index / _length;
    _startingPos = widget.index / _length;
    _rotation = 0;
    _animationController = AnimationController(vsync: this, value: _pos);
    _rotationController = AnimationController(vsync: this, value: _rotation);

    _secondAnimation = Tween(
      begin: 0.2,
      end: 1.0,
    ).animate(
      CurvedAnimation(parent: _rotationController, curve: Curves.linear),
    )..addListener(() {
        setState(() {
          _rotation = _secondAnimation.value;
        });
      });

    _animationController.addListener(() {
      setState(() {
        _pos = _animationController.value;
        final endingPos = _endingIndex / widget.items.length;
        final middle = (endingPos + _startingPos) / 2;
        if ((endingPos - _pos).abs() < (_startingPos - _pos).abs()) {
          _icon = widget.items[_endingIndex];
          _actionIcon = widget.actions[_endingIndex].icon;
          _actionFnc = widget.actions[_endingIndex].fnc;
        }
        _buttonHide =
            (1 - ((middle - _pos) / (_startingPos - middle)).abs()).abs();
      });
    });
  }

  @override
  void didUpdateWidget(NavigationBar oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (oldWidget.index != widget.index) {
      final newPosition = widget.index / _length;
      _rotationController.animateTo(1,
          duration: Duration(microseconds: 1), curve: Curves.linear);
      _startingPos = _pos;
      _endingIndex = widget.index;
      _animationController
          .animateTo(newPosition,
              duration: widget.animationDuration, curve: widget.animationCurve)
          .then((value) => _rotationController.animateTo(0,
              duration: widget.actionDuration, curve: widget.animationCurve));
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    _rotationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      color: widget.backgroundColor,
      height: widget.height + 60,
      child: Flex(
          direction: Axis.vertical,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              color: widget.backgroundColor,
              height: 60,
              child: Stack(
                children: <Widget>[
                  Positioned(
                    bottom: 10 - (50.0 - widget.height),
                    left: Directionality.of(context) == TextDirection.rtl
                        ? null
                        : _pos * size.width,
                    right: Directionality.of(context) == TextDirection.rtl
                        ? _pos * size.width
                        : null,
                    width: size.width / _length,
                    child: _actionIcon != null && _actionFnc != null
                        ? Center(
                            child: InkWell(
                              onTap: _actionFnc,
                              child: Transform.rotate(
                                angle: pi,
                                child: Transform(
                                  transform: Matrix4.diagonal3Values(
                                      _rotation < 1 ? 1 : 0, 1, 1)
                                    ..rotateX(_rotation * 1.5),
                                  child: Material(
                                    color: widget.buttonBackgroundColor ??
                                        widget.color,
                                    shape: BeveledRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            bottomLeft:
                                                Radius.elliptical(25, 15),
                                            bottomRight:
                                                Radius.elliptical(25, 15))),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Transform.rotate(
                                          angle: pi, child: _actionIcon),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Center(),
                  ),
                ],
              ),
            ),
            Container(
              color: widget.backgroundColor,
              height: 50,
              child: Stack(
                overflow: Overflow.visible,
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  Positioned(
                    bottom: -40 - (75.0 - widget.height),
                    left: Directionality.of(context) == TextDirection.rtl
                        ? null
                        : _pos * size.width,
                    right: Directionality.of(context) == TextDirection.rtl
                        ? _pos * size.width
                        : null,
                    width: size.width / _length,
                    child: Center(
                      child: Transform.translate(
                        offset: Offset(
                          0,
                          -(1 - _buttonHide) * 80 + 5,
                        ),
                        child: Material(
                          color: widget.buttonBackgroundColor ?? widget.color,
                          shape: BeveledRectangleBorder(
                              borderRadius: BorderRadius.only(
                                  bottomRight: Radius.elliptical(25, 15),
                                  bottomLeft: Radius.elliptical(25, 15))),
                          child: SizedBox(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: _icon,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0 - (75.0 - widget.height),
                    child: CustomPaint(
                      painter: NavCustomPainter(_pos, _length, widget.color,
                          Directionality.of(context)),
                      child: Container(
                        height: 75.0,
                      ),
                    ),
                  ),
                  Positioned(
                    left: 0,
                    right: 0,
                    bottom: 0 - (45.0 - widget.height),
                    child: SizedBox(
                        height: 40.0,
                        child: Row(
                            children: widget.items.map((item) {
                          return NavButton(
                            onTap: _buttonTap,
                            position: _pos,
                            length: _length,
                            index: widget.items.indexOf(item),
                            child: item,
                          );
                        }).toList())),
                  ),
                ],
              ),
            ),
          ]),
    );
  }

  void setPage(int index) {
    _buttonTap(index);
  }

  void _buttonTap(int index) {
    if (_endingIndex != index) {
      if (widget.onTap != null) {
        widget.onTap(index);
      }
      final newPosition = index / _length;
      setState(() {
        _rotationController.animateTo(1,
            duration: Duration(microseconds: 1), curve: Curves.linear);
        _startingPos = _pos;
        _endingIndex = index;
        _animationController
            .animateTo(newPosition,
                duration: widget.animationDuration,
                curve: widget.animationCurve)
            .then((value) => _rotationController.animateTo(0,
                duration: widget.actionDuration, curve: widget.animationCurve));
      });
    }
  }
}
