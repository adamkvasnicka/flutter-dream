import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/cupertino.dart';

class DialogFlareActor extends StatelessWidget {
  final String flarePath;
  final String animation;

  DialogFlareActor(this.flarePath, this.animation);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: FlareActor(
            flarePath,
            animation: animation,
            alignment: Alignment.center,
            fit: BoxFit.scaleDown,
            sizeFromArtboard: true,
            isPaused: false,
          ),
        ),
      ],
    );
  }
}
