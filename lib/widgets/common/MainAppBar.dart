import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MainAppBar extends StatelessWidget implements PreferredSizeWidget {
  Widget title;

  MainAppBar({this.title});

  @override
  Widget build(BuildContext context) {
    return AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true,
        title: title);
  }

  @override
  Size get preferredSize {
    return new Size.fromHeight(50.0);
  }
}
