import 'package:coffee_debtor/models/CoffeeList.dart';
import 'package:coffee_debtor/notifiers/AuthNotifier.dart';
import 'package:coffee_debtor/models/NavigationAction.dart';
import 'package:coffee_debtor/notifiers/RouteNotifier.dart';
import 'package:coffee_debtor/widgets/CoffeeQuery.dart';
import 'package:coffee_debtor/widgets/bottomNavigation/NavigationBar.dart';
import 'package:coffee_debtor/widgets/dialogs/CakeDialogFunction.dart';
import 'package:coffee_debtor/widgets/dialogs/CoffeeDialogFunction.dart';
import 'package:coffee_debtor/widgets/network_connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

import 'MainAppBar.dart';

final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

class AppScaffold extends StatelessWidget {
  final CoffeeQueryWidgetCallback body;
  final Widget title;

  AppScaffold({this.body, this.title});

  @override
  Widget build(BuildContext context) {
    Widget defaultTitle = Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text(
          "Coffee Debtor",
          style: TextStyle(color: Colors.white),
        ),
        ConnectivityCheck(),
      ],
    );
    String jwtToken = Provider.of<AuthNotifier>(context).session.accessToken.jwtToken;
    return CoffeeQuery(
      widget: (CoffeeList items, Function() refetch) => Scaffold(
        key: scaffoldKey,
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: MainAppBar(title: title != null ? title : defaultTitle),
        body: body(items, refetch),
        bottomNavigationBar: NavigationBar(
          index: Provider.of<RouteNotifier>(context).currentIndex,
          onTap: (i) =>
              Provider.of<RouteNotifier>(context, listen: false).changeRoute(i),
          color: Theme.of(context).primaryColor,
          backgroundColor: Theme.of(context).backgroundColor,
          actions: <NavigationAction>[
            NavigationAction(
              () => showCoffeeDialog(context, jwtToken, refetch),
              Icon(
                Icons.add,
                size: 30,
                color: Theme.of(context).primaryIconTheme.color,
              ),
            ),
            NavigationAction(
              () => showCakeDialog(context, jwtToken, refetch),
              Icon(
                Icons.add,
                size: 30,
                color: Theme.of(context).primaryIconTheme.color,
              ),
            ),
            NavigationAction(null, null),
            NavigationAction(null, null),
          ],
          items: <Widget>[
            SizedBox(
              height: 30,
              width: 30,
              child: SvgPicture.asset(
                "assets/mug.svg",
                color: Theme.of(context).primaryIconTheme.color,
              ),
            ),
            Icon(
              Icons.cake,
              size: 30,
              color: Theme.of(context).primaryIconTheme.color,
            ),
            Icon(
              Icons.compare_arrows,
              size: 30,
              color: Theme.of(context).primaryIconTheme.color,
            ),
            Icon(
              Icons.settings,
              size: 30,
              color: Theme.of(context).primaryIconTheme.color,
            ),
          ],
          animationDuration: Duration(milliseconds: 400),
        ),
      ),
    );
  }
}
