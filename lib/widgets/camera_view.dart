import 'dart:io';

import 'package:camera/camera.dart';
import 'package:coffee_debtor/main.dart';
import 'package:coffee_debtor/widgets/common/AppScaffold.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';

class CameraView extends StatefulWidget {
  @override
  _CameraViewState createState() => _CameraViewState();
}

class _CameraViewState extends State<CameraView> {
  CameraController controller;

  @override
  void initState() {
    super.initState();
    controller = CameraController(cameras[0], ResolutionPreset.medium);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  void _showCameraException(CameraException e) {
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }

  Future<String> _takePicture() async {
    if (!controller.value.isInitialized) {
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 120,
      height: 468,
      child: Conditional.single(
          context: context,
          conditionBuilder: (_) => !controller.value.isInitialized,
          widgetBuilder: (_) => SizedBox.shrink(),
          fallbackBuilder: (_) => Column(
                children: [
                  AspectRatio(
                      aspectRatio: controller.value.aspectRatio,
                      child: CameraPreview(controller)),
                  Row(
                    children: [
                      Flexible(
                        child: Container(
                          color: Theme.of(context).primaryColor,
                          child: Center(
                            child: FlatButton.icon(
                                onPressed: () async {
                                  String filePath = await _takePicture();
                                  if (filePath != null)
                                    showInSnackBar(
                                        'Picture saved to $filePath');
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(Icons.camera,
                                    color: Theme.of(context).accentColor),
                                label: Text('Save memory',
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .body2
                                        .copyWith(
                                            color: Theme.of(context)
                                                .accentColor))),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              )),
    );
  }
}
