import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

import '../models/CoffeeList.dart';
import '../graphql/queries/queries.dart';

typedef CoffeeQueryWidgetCallback = Widget Function(CoffeeList items, Function() refetch);

class CoffeeQuery extends StatelessWidget {
  CoffeeQueryWidgetCallback widget;

  CoffeeQuery({Key key, this.widget}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(documentNode: gql(listCoffees)),
      builder: (
        QueryResult result, {
        VoidCallback refetch,
        FetchMore fetchMore,
      }) {
        if (result.hasException) {
          return Text(result.exception.toString());
        }
        if (result.loading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
        CoffeeList coffees = CoffeeList.fromJson(result.data['listCoffees'])
          ..sortItems();
        return this.widget(coffees, refetch);
      },
    );
  }
}
