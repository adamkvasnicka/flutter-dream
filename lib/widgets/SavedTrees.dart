import 'package:coffee_debtor/models/CoffeeList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

var AVERAGE_TREE_RECEIPTS = 55000;

class SavedTrees extends StatelessWidget {
  double savedTrees;

  SavedTrees({CoffeeList coffeeList})
      : savedTrees = coffeeList.getNotDeletedCoffees().length / 55000;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      child: ListTile(
        trailing: Text('${savedTrees.toStringAsFixed(5)}',
            style: Theme.of(context).primaryTextTheme.body1),
        title: Text('Saved trees',
            style: Theme.of(context).primaryTextTheme.body1),
      ),
    );
  }
}
