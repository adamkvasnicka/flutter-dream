import 'package:coffee_debtor/models/Coffee.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'OrderTile.dart';

class OrderList extends StatelessWidget {
  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);

  List<Coffee> items = List<Coffee>();
  String heading;
  final Future<QueryResult> Function() refetch;

  OrderList({this.items, this.heading, this.refetch});

  @override
  Widget build(BuildContext context) {
    refresh() async {
      await refetch();
      _refreshController.refreshCompleted();
    }

    return Column(
      children: [
        Padding(
          padding: EdgeInsets.all(20.0),
          child: Text(heading,
              style: TextStyle(
                fontSize: 30,
                color: Theme.of(context).primaryTextTheme.body2.color,
              )),
        ),
        Expanded(
          child: SmartRefresher(
            enablePullDown: true,
            enablePullUp: true,
            header:
                WaterDropHeader(waterDropColor: Theme.of(context).primaryColor),
            controller: _refreshController,
            onRefresh: refresh,
            onLoading: refresh,
            child: ListView.builder(
              itemCount: items.length,
              itemBuilder: (context, index) {
                Coffee coffee = items[index];
                return OrderTile(refetch: refetch, coffee: coffee);
              },
            ),
          ),
        ),
      ],
    );
  }
}
