import 'package:coffee_debtor/models/CoffeeList.dart';
import 'package:coffee_debtor/notifiers/AuthNotifier.dart';
import 'package:coffee_debtor/notifiers/RouteNotifier.dart';
import 'package:coffee_debtor/widgets/CoffeeQuery.dart';
import 'package:coffee_debtor/widgets/common/AppScaffold.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../ClientProvider.dart';

class CoffeeDebtor extends StatefulWidget {
  final RouteNotifier routes;

  CoffeeDebtor(this.routes);

  @override
  _CoffeeDebtorState createState() => _CoffeeDebtorState();
}

class _CoffeeDebtorState extends State<CoffeeDebtor>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: routes.length);
    _tabController.index = widget.routes.currentIndex;
    _tabController.addListener(() {
      widget.routes.changeRoute(_tabController.index);
    });
  }

  @override
  void didUpdateWidget(CoffeeDebtor oldWidget) {
    super.didUpdateWidget(oldWidget);
    _tabController.index = widget.routes.currentIndex;
  }

  Widget build(BuildContext context) {
    return ClientProvider(
      accessToken:
          Provider.of<AuthNotifier>(context).session.accessToken.jwtToken,
      child: AppScaffold(
          body: (CoffeeList items, Function() refetch) => TabBarView(
                controller: _tabController,
                children: routes.map((CoffeeQueryWidgetCallback tab) {
                  return Tab(
                    child: tab(items, refetch),
                  );
                }).toList(),
              )),
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }
}
