import 'package:coffee_debtor/abstract/CoffeeQueryConsumer.dart';
import 'package:coffee_debtor/graphql/mutations/mutations.dart';
import 'package:flutter/widgets.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

typedef MutationBuilder = Widget Function(
    RunMutation runMutation,
    );

class DebtorMutation extends CoffeeQueryConsumer {
  final MutationBuilder builder;
  final VoidCallback onCompleted;

  DebtorMutation({final Key key, this.builder, this.onCompleted});

  @override
  Widget build(BuildContext context) {
    return Mutation(
        options: MutationOptions(
            documentNode: gql(createOrder),
            onError: (OperationException exception) => print(exception),
            onCompleted: (resultData) => onCompleted()),
        builder: (
          RunMutation runMutation,
          QueryResult result,
        ) =>
            builder(runMutation));
  }
}
