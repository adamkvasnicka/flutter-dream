import 'package:coffee_debtor/widgets/camera_view.dart';
import 'package:flutter/material.dart';

Future showCameraDialog(BuildContext context) => showDialog(
    context: context,
    builder: (BuildContext context) {
      return SimpleDialog(
          backgroundColor: Colors.transparent, children: [CameraView()]);
    });
