import 'package:coffee_debtor/widgets/dialogs/CameraDialog.dart';
import 'package:coffee_debtor/widgets/dialogs/DebtorMutation.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:uuid/uuid.dart';

import '../../ClientProvider.dart';
import '../DialogFlareActor.dart';
import '../GiffyDialog.dart';

var uuid = Uuid();

Map<String, dynamic> createCoffee(debtorId, Position position) => {
      'id': uuid.v4(),
      'debtorId': debtorId,
      'withCoffee': true,
      'withCake': false,
      'date': DateTime.now().toString(),
      'latitude': position?.latitude?.toString() ?? '',
      'longitude': position?.longitude?.toString() ?? '',
    };

Future showCoffeeDialog(
        BuildContext context, String jwtToken, Function() refetch) =>
    showDialog(
      context: context,
      builder: (BuildContext context) => ClientProvider(
        accessToken: jwtToken,
        child: DebtorMutation(
          onCompleted: () {
            refetch();
          },
          builder: (runMutation) => GiffyDialog(
            flareActor: DialogFlareActor('assets/CoffeMachine.flr', 'loading'),
            title: 'Who paid for coffee',
            buttonCancelText: 'Vladimír',
            buttonOkText: 'Adam',
            onCancelButtonPressed: () async {
              Position position = await Geolocator()
                  .getCurrentPosition(desiredAccuracy: LocationAccuracy.lowest);
              runMutation(createCoffee(2, position));
              Navigator.of(context).pop();
              showCameraDialog(context);
            },
            onOkButtonPressed: () async {
              Position position = await Geolocator()
                  .getCurrentPosition(desiredAccuracy: LocationAccuracy.lowest);
              runMutation(createCoffee(1, position));
              Navigator.of(context).pop();
              showCameraDialog(context);
            },
          ),
        ),
      ),
    );
