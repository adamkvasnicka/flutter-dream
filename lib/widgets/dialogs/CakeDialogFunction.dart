import 'package:coffee_debtor/widgets/dialogs/CameraDialog.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:uuid/uuid.dart';

import '../../ClientProvider.dart';
import '../CoffeeQuery.dart';
import '../DialogFlareActor.dart';
import '../GiffyDialog.dart';
import 'DebtorMutation.dart';

var uuid = Uuid();

Map<String, dynamic> createCake(debtorId, Position position) => {
      'id': uuid.v4(),
      'debtorId': debtorId,
      'withCoffee': false,
      'withCake': true,
      'date': DateTime.now().toString(),
      'latitude': position?.latitude?.toString(),
      'longitude': position?.longitude?.toString(),
    };

Future showCakeDialog(
        BuildContext context, String jwtToken, Function() refetch) =>
    showDialog(
        context: context,
        builder: (BuildContext context) => ClientProvider(
              accessToken: jwtToken,
              child: DebtorMutation(
                onCompleted: () {
                  refetch();
                },
                builder: (runMutation) => GiffyDialog(
                  flareActor: DialogFlareActor('assets/cake.flr', 'Cake'),
                  title: 'Who paid for cake',
                  buttonCancelText: 'Vladimír',
                  buttonOkText: 'Adam',
                  onCancelButtonPressed: () async {
                    Position position = await Geolocator().getCurrentPosition(
                        desiredAccuracy: LocationAccuracy.lowest);
                    runMutation(createCake(1, position));
                    Navigator.of(context).pop();
                    showCameraDialog(context);
                  },
                  onOkButtonPressed: () async {
                    Position position = await Geolocator().getCurrentPosition(
                        desiredAccuracy: LocationAccuracy.lowest);
                    runMutation(createCake(2, position));
                    Navigator.of(context).pop();
                    showCameraDialog(context);
                  },
                ),
              ),
            ));
