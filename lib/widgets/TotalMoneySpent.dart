import 'package:coffee_debtor/models/CoffeeList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// TODO: Move into settings
var DEFAULT_COFFEE_PRICE = 69;
var DEFAULT_CAKE_PRICE = 65;

class TotalMoneySpent extends StatelessWidget {
  int totalSpentOnCoffee = 0;
  int totalSpentOnCakes = 0;

  TotalMoneySpent({CoffeeList coffeeList})
      : totalSpentOnCoffee =
            coffeeList.getWithCoffee().length * DEFAULT_COFFEE_PRICE,
        totalSpentOnCakes =
            coffeeList.getWithCake().length * DEFAULT_CAKE_PRICE;

  @override
  Widget build(BuildContext context) {
    List<Map<String, String>> items = [
      {'text': 'Coffees', 'value': '${totalSpentOnCoffee.toString()} Kč'},
      {'text': 'Cakes', 'value': '${totalSpentOnCakes.toString()} Kč'},
      {
        'text': 'Total',
        'value': '${(totalSpentOnCakes + totalSpentOnCoffee).toString()} Kč'
      }
    ];
    return Column(
        children: items
            .map((item) => Container(
                  height: 50,
                  child: ListTile(
                    title: Text(item['text'],
                        style: Theme.of(context).primaryTextTheme.body1),
                    trailing: Text(item['value'],
                        style: Theme.of(context).primaryTextTheme.body1),
                  ),
                ))
            .toList());
  }
}
