import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:giffy_dialog/giffy_dialog.dart';

import 'DialogFlareActor.dart';

class GiffyDialog extends StatelessWidget {
  final String title;
  final String buttonCancelText;
  final String buttonOkText;
  final Function onCancelButtonPressed;
  final Function onOkButtonPressed;
  final DialogFlareActor flareActor;

  GiffyDialog(
      {this.title,
      this.buttonCancelText,
      this.buttonOkText,
      this.onCancelButtonPressed,
      this.onOkButtonPressed,
      this.flareActor});

  Widget build(BuildContext context) {
    return NetworkGiffyDialog(
      image: flareActor,
      title: Text(
        title,
        style: TextStyle(
          fontSize: 30,
          color: Theme.of(context).primaryTextTheme.body2.color,
        ),
      ),
      buttonCancelText: Text(buttonCancelText,
          style: TextStyle(
              fontSize: 20,
              color: Theme.of(context).primaryTextTheme.body2.color)),
      buttonOkText: Text(buttonOkText,
          style: TextStyle(
              fontSize: 20,
              color: Theme.of(context).primaryTextTheme.body2.color)),
      onCancelButtonPressed: onCancelButtonPressed,
      onOkButtonPressed: onOkButtonPressed,
      buttonOkColor: Theme.of(context).backgroundColor,
      buttonCancelColor: Theme.of(context).backgroundColor,
    );
  }
}
