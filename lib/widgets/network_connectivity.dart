import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ConnectivityCheck extends StatefulWidget {
  @override
  _ConnectivityCheckState createState() => _ConnectivityCheckState();
}

class _ConnectivityCheckState extends State<ConnectivityCheck> {
  StreamSubscription<ConnectivityResult> subscription;
  ConnectivityResult result = ConnectivityResult.mobile;

  @override
  initState() {
    super.initState();
    subscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult connectivityResult) {
      setState(() {
        result = connectivityResult;
      });
    });
    Connectivity()
        .checkConnectivity()
        .then((ConnectivityResult connectivityResult) {
      setState(() {
        result = connectivityResult;
      });
    });
  }

  @override
  dispose() {
    super.dispose();
    subscription.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Icon(
      result == ConnectivityResult.none
          ? Icons.signal_cellular_null
          : result == ConnectivityResult.mobile
              ? Icons.signal_cellular_4_bar
              : Icons.wifi,
      size: 30,
      color: Colors.white,
    ));
  }
}
