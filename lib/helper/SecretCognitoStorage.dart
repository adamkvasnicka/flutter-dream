import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecretCognitoStorage extends CognitoStorage {
  final _storage = new FlutterSecureStorage();

  @override
  Future setItem(String key, value) async {
    return _storage.write(key: key, value: value);
  }

  @override
  Future getItem(String key) async {
    return _storage.read(key: key);
  }

  @override
  Future removeItem(String key) async {
    return _storage.delete(key: key);
  }

  @override
  Future<void> clear() async {
    return _storage.deleteAll();
  }
}
