import 'package:coffee_debtor/notifiers/AuthNotifier.dart';
import 'package:coffee_debtor/notifiers/SecurityNotifier.dart';
import 'package:coffee_debtor/notifiers/ThemeNotifier.dart';
import 'package:day_night_switch/day_night_switch.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const dayColor = Color(0xFFd56352);
const nightColor = Color(0xFF1e2230);

class SettingsScreen extends StatelessWidget {
  SettingsScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: Text('Settings',
                style: TextStyle(
                  fontSize: 30,
                  color: Theme.of(context).primaryTextTheme.body2.color,
                )),
          ),
          Expanded(
            child: ListView(
              children: [
                Consumer<ThemeNotifier>(
                  builder: (context, theme, _) => ListTile(
                    onTap: theme.switchTheme,
                    title: Text('Theme',
                        style: Theme.of(context).primaryTextTheme.body1),
                    trailing: Transform.scale(
                      scale: 0.5,
                      child: DayNightSwitch(
                        value: theme.actualTheme == ThemeTypes.DARK,
                        moonImage: AssetImage('assets/moon.png'),
                        dayColor: dayColor,
                        nightColor: nightColor,
                        onChanged: (value) {
                          theme.switchTheme();
                        },
                      ),
                    ),
                  ),
                ),
                Consumer<SecurityNotifier>(
                  builder: (context, security, _) => ListTile(
                    onTap: security.toggleSecurity,
                    title: Text('Biometrics',
                        style: Theme.of(context).primaryTextTheme.body1),
                    trailing: Padding(
                      padding: EdgeInsets.only(right: 25),
                      child: Transform.scale(
                        scale: 1.3,
                        child: CupertinoSwitch(
                          onChanged: (bool newValue) =>
                              security.toggleSecurity(),
                          value: security.getSecurityEnabled(),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10),
            child: SizedBox(
              height: 40,
              width: double.infinity,
              child: Consumer<AuthNotifier>(
                builder: (context, auth, _) => FlatButton(
                  onPressed: auth.clearSession,
                  color: Theme.of(context).primaryColor,
                  child: Text("Logout",
                      style: TextStyle(color: Theme.of(context).accentColor)),
                  shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(7))),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
