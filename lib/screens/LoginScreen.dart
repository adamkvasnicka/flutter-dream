import 'package:amazon_cognito_identity_dart_2/cognito.dart';
import 'package:coffee_debtor/notifiers/AuthNotifier.dart';
import 'package:flutter/material.dart';
import 'package:flutter_conditional_rendering/conditional.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool requireNewPassword = false;
  bool loading = false;
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  String error = '';
  final _formKey = GlobalKey<FormState>();

  onPress(context, AuthNotifier auth) {
    if (_formKey.currentState.validate() && !loading) {
      setState(() {
        error = '';
      });
      login(auth);
    }
  }

  setLoading(bool loading) {
    setState(() {
      this.loading = loading;
    });
  }

  setError(String message) {
    setState(() {
      error = message;
    });
  }

  login(AuthNotifier auth) {
    setLoading(true);
    CognitoUser cognitoUser = auth.createUser(usernameController.text);
    var authenticationDetails = AuthenticationDetails(
        username: usernameController.text, password: passwordController.text);
    cognitoUser
        .authenticateUser(authenticationDetails)
        .then((session) => auth.setSession(session))
        .then((_) => setLoading(false))
        .catchError((e) => setError(e.message))
        .whenComplete(() => setLoading(false));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Consumer<AuthNotifier>(
        builder: (context, auth, _) => Center(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 25),
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Flex(
                  direction: Axis.vertical,
                  children: <Widget>[
                    SizedBox(
                      height: 40,
                    ),
                    Center(
                      child: SvgPicture.asset(
                        "assets/logo.svg",
                        semanticsLabel: 'Logo',
                        color: Theme.of(context).accentIconTheme.color,
                        width: 150,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: usernameController,
                      style: Theme.of(context).primaryTextTheme.body2,
                      decoration: InputDecoration(
                        hintText: 'Enter your username',
                        labelText: 'username',
                        hintStyle: Theme.of(context).primaryTextTheme.body2,
                        labelStyle:
                            Theme.of(context).primaryTextTheme.body2,
                        border: Theme.of(context).inputDecorationTheme.border,
                        focusedBorder:
                            Theme.of(context).inputDecorationTheme.border,
                        enabledBorder:
                            Theme.of(context).inputDecorationTheme.border,
                      ),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter username';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      controller: passwordController,
                      style: Theme.of(context).primaryTextTheme.body2,
                      decoration: InputDecoration(
                        hintText: 'Enter your password',
                        labelText: 'password',
                        hintStyle: Theme.of(context).primaryTextTheme.body2,
                        labelStyle:
                            Theme.of(context).primaryTextTheme.body2,
                        border: Theme.of(context).inputDecorationTheme.border,
                        focusedBorder:
                            Theme.of(context).inputDecorationTheme.border,
                        enabledBorder:
                            Theme.of(context).inputDecorationTheme.border,
                      ),
                      obscureText: true,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter password';
                        }
                        return null;
                      },
                    ),
                    Text(
                      error,
                      style: TextStyle(color: Colors.red, fontSize: 14),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        FlatButton(
                          onPressed:
                              loading ? null : () => onPress(context, auth),
                          child: Conditional.single(
                              context: context,
                              conditionBuilder: (context) => loading,
                              widgetBuilder: (context) => SizedBox(
                                  width: 20,
                                  height: 20,
                                  child: CircularProgressIndicator()),
                              fallbackBuilder: (context) => Text("Login")),
                          color: Colors.brown[300],
                          shape: BeveledRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(7))),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    usernameController.dispose();
    passwordController.dispose();
    super.dispose();
  }
}
