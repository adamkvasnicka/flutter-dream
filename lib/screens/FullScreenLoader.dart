import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class FullScreenLoader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
              child: SvgPicture.asset(
                "assets/logo.svg",
                semanticsLabel: 'Logo',
                color: Theme.of(context).accentIconTheme.color,
                width: 150,
              ),
            ),
            SizedBox(
              height: 500,
              child: FlareActor(
                "assets/CoffeMachine.flr",
                animation: "loading",
                alignment: Alignment.center,
                fit: BoxFit.scaleDown,
                sizeFromArtboard: true,
              ),
            )
          ],
        ));
  }
}
