import 'package:coffee_debtor/notifiers/RouteNotifier.dart';
import 'package:coffee_debtor/widgets/CoffeeDebtor.dart';
import 'package:coffee_debtor/service/LocalAuthenticationService.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BiometricsScreen extends StatefulWidget {
  @override
  _BiometricsScreenState createState() => _BiometricsScreenState();
}

class _BiometricsScreenState extends State<BiometricsScreen> {
  final LocalAuthenticationService _localAuthentication =
      locator<LocalAuthenticationService>();

  bool _didAuthenticate = false;

  @override
  void initState() {
    super.initState();
    this._authorize();
  }

  void _authorize() async {
    var didAuthenticate = await _localAuthentication.authenticate();
    setState(() => _didAuthenticate = didAuthenticate);
  }

  Widget build(BuildContext context) {
    if (_didAuthenticate) {
      return ChangeNotifierProvider<RouteNotifier>(
          create: (_) => RouteNotifier(),
          child: Consumer<RouteNotifier>(
              builder: (context, _routes, _) => CoffeeDebtor(_routes)));
    }
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Container(),
    );
  }
}
