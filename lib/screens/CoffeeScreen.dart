import 'package:coffee_debtor/models/CoffeeList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../widgets/OrderList.dart';
import '../models/Coffee.dart';
import '../abstract/CoffeeQueryConsumer.dart';

class CoffeeScreen extends CoffeeQueryConsumer {
  CoffeeList coffeeList;
  Function() refetch;

  CoffeeScreen({Key key, this.coffeeList, this.refetch})
      : super(key: key, coffeeList: coffeeList, refetch: refetch);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: OrderList(
          items: coffeeList
              .getNotDeletedCoffees()
              .where((Coffee item) => item.withCoffee ?? false)
              .toList(),
          heading: 'Coffee',
          refetch: refetch),
    );
  }
}
