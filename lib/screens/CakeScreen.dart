import 'package:coffee_debtor/models/CoffeeList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../widgets/OrderList.dart';
import '../models/Coffee.dart';
import '../abstract/CoffeeQueryConsumer.dart';

class CakeScreen extends CoffeeQueryConsumer {
  CoffeeList coffeeList;
  Function() refetch;

  CakeScreen({Key key, this.coffeeList, this.refetch})
      : super(key: key, coffeeList: coffeeList, refetch: refetch);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: OrderList(
          items: coffeeList
              .getNotDeletedCoffees()
              .where((Coffee item) => item.withCake ?? false)
              .toList(),
          heading: 'Cake',
          refetch: refetch),
    );
  }
}
