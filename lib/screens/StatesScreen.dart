import 'package:coffee_debtor/models/CoffeeList.dart';
import 'package:coffee_debtor/widgets/SavedTrees.dart';
import 'package:coffee_debtor/widgets/SpendingChart.dart';
import 'package:coffee_debtor/widgets/TotalMoneySpent.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../abstract/CoffeeQueryConsumer.dart';

class StatesScreen extends CoffeeQueryConsumer {
  CoffeeList coffeeList;
  Function() refetch;

  StatesScreen({Key key, this.coffeeList, this.refetch})
      : super(key: key, coffeeList: coffeeList, refetch: refetch);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).backgroundColor,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Text('Spending',
                style: TextStyle(
                  fontSize: 30,
                  color: Theme.of(context).primaryTextTheme.body2.color,
                )),
          ),
          Expanded(
            child: ListView(
              children: [
                SpendingChart(coffeeList: coffeeList),
                TotalMoneySpent(coffeeList: coffeeList),
                SavedTrees(coffeeList: coffeeList)
              ],
            ),
          ),
        ],
      ),
    );
  }
}
